# Merge request summary

{Describe the purpose of your merge request. For example: "Adds the abc template to the repository."}

IMPORTANT: The next three sections in this checklist should be filled out by the template contributor at the time they submit this request. Merge requests can only be merged when all boxes are checked.


## Which issue does this merge request fix or reference?

This merge request:

- Resolves: {#issue-number} {Choose this option if this MR will close the issue and link to issue}
- Relates to: {#issue-number} {Choose this option if this MR only references an issue for context but does not fully resolve it}


## Working group details

{Indicate which template working group you belong to and who your template working group lead is.}

- [ ] One or more of my working group leads approved my template project to move to the merge request phase.
- [ ] The template contributor participated in a template working group to create this template.
- [ ] This template was submitted to the community for feedback.



## Template deliverables requirements

- [ ] Template file is present.
- [ ] Template guide is present.
- [ ] Template resources is present.
- [ ] Template process is present.


## Checklist for reviewers

IMPORTANT: The rest of the sections in this checklist should only be filled out by authorized Good Docs Project merge request reviewers. If you are the individual template contributor, do not fill out the rest of the fields or check the boxes.

NOTE: Merge requests can only be merged when all boxes are checked.


### Mechanics and formatting requirements - MERGE REQUEST REVIEWER ONLY

- [ ] Check rendered Markdown output to ensure it renders correctly.
- [ ] Review the template set to ensure all documents follow the template markdown style guide.
- [ ] The template set is free from grammar errors and typos.


### Overall usability - MERGE REQUEST REVIEWER ONLY

- [ ] The scope of the template set is appropriate, meaning it is not too simple or overly complex (i.e. it needs to be expanded or broken into multiple smaller templates).
- [ ] The template is complete and comprehensive.
- [ ] The template set is well-organized and the contents flow in a logical order.
- [ ] The template is well-written and clear.
- [ ] The template set provides sufficient guidance about how to fill it in and implement the template in a documentation project.


### Template file requirements - MERGE REQUEST REVIEWER ONLY

- [ ] Check rendered Markdown output to ensure it renders correctly and follows the template markdown style guide.
- [ ] The template title is present and is an H1.
- [ ] The template includes an introductory comment mentioning the other template resources (such as the guide).
- [ ] Embedded writing tips are formatted with {curly brackets}.
- [ ] The template is free from grammar errors and typos.
- [ ] The scope of the template is appropriate, meaning it is not too simple or overly complex (i.e. it needs to be expanded or broken into multiple smaller templates).
- [ ] The template is complete and comprehensive.
- [ ] The template is well-organized and the contents flow in a logical order.
- [ ] The template is well-written and clear.


### Template guide requirements - MERGE REQUEST REVIEWER ONLY

- [ ] Check rendered Markdown output to ensure it renders correctly and follows the template markdown style guide.
- [ ] The template guide title is present and is an H1.
- [ ] The template includes an introductory comment pointing them to other template resources.
- [ ] Embedded writing tips are formatted with {curly brackets}.
- [ ] The template guide is free from grammar errors and typos.
- [ ] The template guide includes a "Why do I need this type of document?" section.
- [ ] The template guide includes a "Contents of this template" section.
- [ ] The contents listed in the "Contents of this template" section mirror the sections in the raw template.
- [ ] The template guide is well-organized and the contents flow in a logical order.
- [ ] The template guide is well-written, clear, and provides sound advice to template users.


### Template resources requirements - MERGE REQUEST REVIEWER ONLY

- [ ] Check rendered Markdown output to ensure it renders correctly and follows the template markdown style guide.
- [ ] The template resources title is present and is an H1.
- [ ] The template includes an introductory comment pointing them to other template resources.
- [ ] Embedded writing tips are formatted with {curly brackets}.
- [ ] The template resources are free from grammar errors and typos.
- [ ] The document includes links to high quality examples that inspired the template.
- [ ] The references include links and descriptions to resources consulted in making the template.
- [ ] The references section describes the lessons you learned and how they were used in the template.
- [ ] The document is well-organized and the contents flow in a logical order.
- [ ] The document is well-written, clear, and provides sound advice to template users.


### Template process requirements - MERGE REQUEST REVIEWER ONLY

- [ ] Check rendered Markdown output to ensure it renders correctly and follows the template markdown style guide.
- [ ] The template resources title is present and is an H1.
- [ ] The template includes an introductory comment pointing them to other template resources.
- [ ] Embedded writing tips are formatted with {curly brackets}.
- [ ] The template resources are free from grammar errors and typos.
- [ ] The document includes guidelines for researching that content type.
- [ ] The document includes guidelines for writing that content type.
- [ ] The document includes guidelines for maintaining that content type.
- [ ] The document is well-organized and the contents flow in a logical order.
- [ ] The document is well-written, clear, and provides sound advice to template users.